package com.shuneault.netrunnerdeckbuilder.game;

public class NetRunnerBD {

    public static final String BASE_URL = "http://netrunnerdb.com";
    public static final String URL_API_SEARCH = "/api/search/";
    public static final String URL_GET_ALL_CARDS = "http://netrunnerdb.com/api/cards/?_locale=%s";

}
